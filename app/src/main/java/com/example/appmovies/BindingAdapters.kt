package com.example.appmovies

import android.view.View
import android.widget.ProgressBar
import androidx.databinding.BindingAdapter
import com.example.appmovies.overview.playingnview.ApiStatus

@BindingAdapter("apiStatus")
fun bindApiStatus(progressBar: ProgressBar, status: ApiStatus?) {
    when(status) {
        ApiStatus.LOADING -> { progressBar.visibility = View.VISIBLE}
        ApiStatus.DONE -> { progressBar.visibility = View.GONE }
        else -> { progressBar.visibility = View.GONE }
    }
}