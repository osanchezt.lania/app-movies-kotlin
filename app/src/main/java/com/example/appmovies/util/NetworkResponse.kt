package com.example.appmovies.util

import com.example.appmovies.model.PopularMovies
import retrofit2.Response

sealed class NetworkResponse {
    object Loading : NetworkResponse()
    class Failure(val msg:Throwable) : NetworkResponse()
    class Success(val data : PopularMovies?) : NetworkResponse()
    object Empty : NetworkResponse()
}
