package com.example.appmovies.util

class Constants {

    companion object {
        const val BASE_URL = "https://api.themoviedb.org/3/movie/"
    }
}