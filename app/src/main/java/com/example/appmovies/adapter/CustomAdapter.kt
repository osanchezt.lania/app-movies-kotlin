package com.example.appmovies.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.appmovies.R
import com.example.appmovies.model.BodyMovies

class CustomAdapter(
    val itemClickListener: ItemClickListener
) : RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    private var moviesList = listOf<BodyMovies>()

    interface ItemClickListener {
        fun onItemClick(movies: BodyMovies)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val itemImage: ImageView
        val title: TextView
        val language: TextView
        val popularity: TextView

        init {
            itemImage = view.findViewById(R.id.item_image)
            title = view.findViewById(R.id.item_title)
            language = view.findViewById(R.id.item_language)
            popularity = view.findViewById(R.id.item_popularity)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =LayoutInflater.from(parent.context)
            .inflate(R.layout.card_item, parent, false)

        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemImage.load("https://image.tmdb.org/t/p/w500" + moviesList[position].poster_path)
        holder.title.text = moviesList[position].original_title
        holder.language.text = "Idioma: " +moviesList[position].original_language
        holder.popularity.text = "Popularidad: " + moviesList[position].popularity.toString()
        holder.itemView.setOnClickListener{
            itemClickListener.onItemClick(moviesList[position])
        }
    }

    override fun getItemCount() = moviesList.size

    @SuppressLint("NotifyDataSetChanged")
    fun setData(newList: List<BodyMovies>) {
        moviesList = newList
        notifyDataSetChanged()
    }

}