package com.example.appmovies.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appmovies.R
import com.example.appmovies.model.Result
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView

class VideoAdapter() : RecyclerView.Adapter<VideoAdapter.ViewHolder>() {

    private var videoList = listOf<Result>()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val youTubePlayer : YouTubePlayerView
        //val vide_key : TextView

        init {
            youTubePlayer = view.findViewById(R.id.youtube_player_view)
            //vide_key = view.findViewById(R.id.vide_key)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.video_item, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        //holder.vide_key.text = videoList[position].key
        holder.youTubePlayer.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) {
                youTubePlayer.cueVideo(
                    videoList[position].key,
                    0f)
            }
        })
    }

    override fun getItemCount() = videoList.size

    fun submitListData(newList : List<Result>) {
        videoList = newList
        notifyDataSetChanged()
    }
}