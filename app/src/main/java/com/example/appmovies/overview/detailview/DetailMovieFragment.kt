package com.example.appmovies.overview.detailview

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.example.appmovies.R
import com.example.appmovies.adapter.VideoAdapter
import com.example.appmovies.databinding.FragmentDetailMovieBinding
import com.example.appmovies.model.Result
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.loadOrCueVideo
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import dagger.hilt.android.AndroidEntryPoint


const val TAG = "DetailFragment"

@AndroidEntryPoint
class DetailMovieFragment : Fragment() {

    private var _binding : FragmentDetailMovieBinding? = null
    private val binding get() = _binding!!
    private val args: DetailMovieFragmentArgs by navArgs()
    private val detailMovieViewModel : DetailMovieViewModel by viewModels()

    private lateinit var videoAdapter : VideoAdapter
    private var videoList : List<Result> = listOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailMovieBinding.inflate(inflater, container, false)
        val view = binding.root

        setupRecyclerView()

        val movie = args.movie
        if (movie.poster_path == null) {
            binding.movieImage.load(R.drawable.ic_baseline_broken)
        } else {
            binding.movieImage.load("https://image.tmdb.org/t/p/w500" + movie.poster_path)
        }
        binding.movieTitle.text = if (movie.title == null)  "No Title Available" else movie.title
        binding.movieRating.rating = if (movie.vote_average != null) movie.vote_average!! / 2 else 0.0f
        binding.movieOverview.text = movie.overview


        detailMovieViewModel.getMovie(movie.id.toString())
        detailMovieViewModel.movie.observe(viewLifecycleOwner) { response ->
            //Log.d(TAG, "${response?.body()}")
        }

        getVideo(movie.id.toString())
        // Inflate the layout for this fragment
        return view
    }

    private fun setupRecyclerView() {
        videoAdapter = VideoAdapter()
        binding.rvVideos.adapter = videoAdapter
        binding.rvVideos.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun getVideo(movie_id: String){
        detailMovieViewModel.getVideo(movie_id)
        detailMovieViewModel.video.observe(viewLifecycleOwner) { video ->
            videoList = video.body()!!.results
            videoAdapter.submitListData(videoList)
        }
    }
}