package com.example.appmovies.overview.detailview

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appmovies.data.Repository
import com.example.appmovies.model.DetailMovie
import com.example.appmovies.model.Videos
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
//import com.example.appmovies.data.network.MoviesApi
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class DetailMovieViewModel @Inject constructor(
    private val repository: Repository
) : ViewModel() {

    private val _movie = MutableLiveData<Response<DetailMovie>?>()
    val movie : LiveData<Response<DetailMovie>?> = _movie

    private val _video = MutableLiveData<Response<Videos>>()
    val video : LiveData<Response<Videos>> = _video

    fun getMovie(movie_id: String) {
        viewModelScope.launch {
            try {
                //val result = MoviesApi.moviesApiService.getMovieById(movie_id)
                //_movie.value = result
            } catch (e: Exception) {
                Log.d("ViewModelDetailMovie", e.message.toString())
            }
        }
    }

    fun getVideo(movie_id: String) {
        viewModelScope.launch {
            repository.getVideo(movie_id)
                .catch { e ->
                    Log.d("ViewModelDetailMovie", e.localizedMessage!!)
                }
                .collect {
                    _video.value = it
                }
        }
    }

    /*fun getVideo(movie_id: String) {
        viewModelScope.launch {
            try {
                //val result = MoviesApi.moviesApiService.getVideo(movie_id)
                //_video.value = result
            } catch (e: Exception) {
                Log.d("ViewModelDetailMovie", e.message.toString())
            }
        }
    }
     */
}