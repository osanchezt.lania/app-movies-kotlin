package com.example.appmovies.overview.popularview

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appmovies.R
import com.example.appmovies.adapter.CustomAdapter
import com.example.appmovies.databinding.FragmentMoviesPopularBinding
import com.example.appmovies.model.BodyMovies
import com.example.appmovies.overview.playingnview.ApiStatus
import com.example.appmovies.util.NetworkResponse
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

const val TAG = "PopularFragment"

@AndroidEntryPoint
class MoviesPopularFragment : Fragment(), CustomAdapter.ItemClickListener {

    private var _binding: FragmentMoviesPopularBinding? = null
    private val binding get() = _binding!!

    private var popularMovies: List<BodyMovies> = listOf()
    private lateinit var adapter : CustomAdapter
    private val moviesPopularViewModel : MoviesPopularViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMoviesPopularBinding.inflate(inflater, container, false)
        val view = binding.root


        checkConnection()
        checkStatus()
        getPopularMovies()
        initRecyclerView(view)
        //loadPopularMovies()

        // Inflate the layout for this fragment
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        moviesPopularViewModel.onCleared()
        _binding = null
    }

    private fun checkConnection() {
        lifecycleScope.launch {
            moviesPopularViewModel.errorCon.collect { error ->
                if (error) {
                    binding.ivErrorConnection.setImageResource(R.drawable.ic_wifi_off)
                    binding.ivErrorConnection.visibility = View.VISIBLE
                    popularMovies = listOf()
                    adapter.setData(popularMovies)
                }
            }
        }
    }

    private fun checkStatus() {
        lifecycleScope.launch {
            moviesPopularViewModel.status.collect { status ->
                if (status != ApiStatus.LOADING) binding.circularProgress.visibility = View.GONE
            }
        }
    }

    private fun getPopularMovies() {
        lifecycleScope.launchWhenStarted {
            moviesPopularViewModel.flowPopularMovies.collect {
                when (it) {
                    is NetworkResponse.Loading -> Log.e(TAG, "Cargando...")
                    is NetworkResponse.Failure -> Log.e(TAG, "Algo salió mal...")
                    is NetworkResponse.Success -> {
                        Log.e(TAG, "${it.data}")
                        popularMovies = it.data!!.results
                        adapter.setData(popularMovies)
                    }
                    else -> {}
                }
            }
        }

        /*
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                moviesPopularViewModel.popularMovies.observe(viewLifecycleOwner) {
                    if (it.isSuccessful) {
                        Log.e(TAG, "${it.body()}")
                        popularMovies = it.body()?.results!!
                        adapter.setData(popularMovies)
                    } else {
                        Log.e(TAG, it.message())
                    }
                }
            }
        }
        */
    }

    private fun initRecyclerView(view: View) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.rv_popular_movies)
        recyclerView?.layoutManager = LinearLayoutManager(context)
        adapter = CustomAdapter(this)
        recyclerView.adapter = adapter
    }

    private fun loadPopularMovies() {
        moviesPopularViewModel.getPopularMovies()
        moviesPopularViewModel.popularMovies.observe(viewLifecycleOwner) { response ->
            if (!response.isSuccessful) {
                Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show()
            } else {
                popularMovies = response.body()?.results!!
                adapter.setData(popularMovies)
            }
        }
    }

    override fun onItemClick(movies: BodyMovies) {
        /*Snackbar.make(requireView(), "Overview: \n ${results.overview}", Snackbar.LENGTH_SHORT)
            .setAnchorView(R.id.bottom_nav_view)
            .setAction("Ok") {}
            .show()*/
        //Toast.makeText(context, "Overview: \n ${movies.overview}", Toast.LENGTH_SHORT).show()
        val action =MoviesPopularFragmentDirections.actionPopularMoviesToDetailMovie(movies)
        findNavController().navigate(action)
    }

}