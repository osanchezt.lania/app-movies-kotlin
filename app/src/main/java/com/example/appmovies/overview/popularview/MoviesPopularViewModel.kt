package com.example.appmovies.overview.popularview

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appmovies.data.Repository
import com.example.appmovies.model.PopularMovies
//import com.example.appmovies.data.network.MoviesApi
import com.example.appmovies.overview.playingnview.ApiStatus
import com.example.appmovies.util.NetworkResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject
import kotlin.Exception

@HiltViewModel
class MoviesPopularViewModel @Inject constructor(
    private val repository: Repository
) : ViewModel() {

    private val _popularMovies = MutableLiveData<Response<PopularMovies>>()
    val popularMovies : LiveData<Response<PopularMovies>> = _popularMovies

    private val _flowPopularMovies = MutableStateFlow<NetworkResponse>(NetworkResponse.Loading)
    val flowPopularMovies : StateFlow<NetworkResponse> = _flowPopularMovies

    //Variables status ProgressBar
    private val _status = MutableStateFlow<ApiStatus>(ApiStatus.LOADING)
    val status: StateFlow<ApiStatus> = _status

    //Variables para mostrar error de red
    private val _errorCon = MutableStateFlow<Boolean>(false)
    val errorCon : StateFlow<Boolean> = _errorCon

    fun getPopularMovies() {
        viewModelScope.launch {
            _status.value = ApiStatus.LOADING
            try {
                //val result = MoviesApi.moviesApiService.getPopularMovies()
                //_popularMovies.value = result
                _status.value = ApiStatus.DONE
                _errorCon.value = false
             } catch (e: Exception) {
                _status.value = ApiStatus.ERROR
                _errorCon.value = true
                Log.d("ViewModelPopularMovies", e.localizedMessage)
             }
        }
    }

    private fun getFlowPopularMovies() {
        viewModelScope.launch {
            _status.value = ApiStatus.LOADING
            _flowPopularMovies.value = NetworkResponse.Loading
            repository.getPopularMovies()
                .catch { exception ->
                    Log.d(TAG, "$exception")
                    _flowPopularMovies.value = NetworkResponse.Failure(exception)
                    _errorCon.value = true
                    _status.value = ApiStatus.ERROR
                }
                .collect {
                    _errorCon.value = false
                    _status.value = ApiStatus.DONE
                    _flowPopularMovies.value = NetworkResponse.Success(it.body())
                }
        }
    }

    /*fun getFlowPopularMovies() {
        viewModelScope.launch {
            networkRepository.getPopularMovies()
                .collect { popularMovies ->
                    if (popularMovies.isSuccessful) _status.value = ApiStatus.DONE
                    _popularMovies.value = popularMovies
            }
        }
    }*/

    init {
        getFlowPopularMovies()
    }

    public override fun onCleared() {
        super.onCleared()
        _errorCon.value = false
    }
}