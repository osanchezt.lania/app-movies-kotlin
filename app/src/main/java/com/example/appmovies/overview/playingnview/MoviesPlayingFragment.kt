package com.example.appmovies.overview.playingnview

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.example.appmovies.adapter.CustomAdapter
import com.example.appmovies.databinding.FragmentMoviesPlayingBinding
import com.example.appmovies.model.BodyMovies
import com.example.appmovies.overview.pagingadapter.MoviesLoadStateAdapter
import com.example.appmovies.overview.pagingadapter.MoviesPagingAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

const val TAG = "PlayFragment"

@AndroidEntryPoint
class MoviesPlayingFragment : Fragment(), MoviesPagingAdapter.ItemClickListener /*CustomAdapter.ItemClickListener*/ {

    private var _binding: FragmentMoviesPlayingBinding? = null
    private val binding get() = _binding!!

    private var movies: List<BodyMovies> = listOf()
    //private lateinit var adapter : CustomAdapter
    private lateinit var movieAdapter : MoviesPagingAdapter
    private val moviesPlayingViewModel : MoviesPlayingViewModel by viewModels() //

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMoviesPlayingBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.moviesPlayingViewModel = moviesPlayingViewModel
        val view =  binding.root

        //loadData()
        //initRecyclerView(binding.root)

        /*movieAdapter = MoviesPagingAdapter(this)
        binding.apply {
            //recyclerView.setHasFixedSize(true)
            recyclerView.adapter = movieAdapter.withLoadStateHeaderAndFooter(
                header = MoviesLoadStateAdapter { movieAdapter.retry() },
                footer = MoviesLoadStateAdapter { movieAdapter.retry() }
            )
            lifecycleScope.launch {
                movieAdapter.loadStateFlow.collectLatest { loadState ->
                    binding.progressBar.isVisible = loadState.refresh is LoadState.Loading
                    //retry.isVisible = loadState.refresh !is LoadState.Loading
                    //errorMsg.isVisible = loadState.refresh is LoadState.Error
                }
            }
        }*/

        initRecycler()
        getPagingData()

        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /*private fun initRecyclerView(view: View) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView?.layoutManager = LinearLayoutManager(context)
        adapter = CustomAdapter(this)
        recyclerView.adapter = adapter
    }*/

    private fun initRecycler() {
        movieAdapter = MoviesPagingAdapter(this)
        binding.recyclerView.adapter = movieAdapter.withLoadStateHeaderAndFooter(
            header = MoviesLoadStateAdapter { movieAdapter.retry() },
            footer = MoviesLoadStateAdapter { movieAdapter.retry() }
        )
        lifecycleScope.launch {
            movieAdapter.loadStateFlow.collectLatest { loadState ->
                binding.progressBar.isVisible = loadState.refresh is LoadState.Loading
                binding.ivStatusConnection.isVisible = loadState.refresh is LoadState.Error
            }
        }
    }

    private fun getPagingData(){
        lifecycleScope.launchWhenCreated {
            moviesPlayingViewModel.getMovies().observe(viewLifecycleOwner) {
                movieAdapter.submitData(lifecycle, it)
            }
        }
    }

    private fun loadData() {
        moviesPlayingViewModel.moviesNowPlaying.observe(viewLifecycleOwner) { response ->
            Log.d(TAG, "${response.body()?.results}")
            movies = response.body()?.results!!
            //adapter.setData(movies)
        }
    }

    override fun onItemClick(movies: BodyMovies) {
        //Toast.makeText(context, "Overview: \n ${results.overview}", Toast.LENGTH_SHORT).show()
        val action = MoviesPlayingFragmentDirections.actionPlayingToDetail(movies)
        findNavController().navigate(action)
    }

}