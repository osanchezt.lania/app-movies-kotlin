package com.example.appmovies.overview.pagingadapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.appmovies.databinding.LayoutLoadingStateBinding

class MoviesLoadStateAdapter(private val retry: () -> Unit) : LoadStateAdapter<MoviesLoadStateAdapter.LoadStateViewHolder>() {

    inner class LoadStateViewHolder(private val binding: LayoutLoadingStateBinding)
        : RecyclerView.ViewHolder(binding.root){


            init {
                binding.btnRetry.setOnClickListener {
                    retry.invoke()
                }
            }

        fun bind(loadState: LoadState) {
            with(binding) {
                pbLoadingState.isVisible = loadState is LoadState.Loading
                btnRetry.isVisible = loadState !is LoadState.Loading
                errorMsg.isVisible = loadState !is LoadState.Loading
            }
        }

        }

    override fun onBindViewHolder(holder: LoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadStateViewHolder {
        val view = LayoutLoadingStateBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LoadStateViewHolder(view)
    }
}