package com.example.appmovies.overview.playingnview

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.example.appmovies.data.Repository
import com.example.appmovies.model.BodyMovies
import com.example.appmovies.model.Movies
import com.example.appmovies.data.network.MoviePagingSource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

enum class ApiStatus { LOADING, DONE, ERROR }

@HiltViewModel
class MoviesPlayingViewModel @Inject constructor (
    private val repository: Repository
        ): ViewModel() {

    private val _moviesNowPlaying = MutableLiveData<Response<Movies>>()
    val moviesNowPlaying : LiveData<Response<Movies>> = _moviesNowPlaying

    //Variables status ProgressBar
    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus> = _status

    fun getMoviesPlaying() {
        viewModelScope.launch {
            _status.value = ApiStatus.LOADING
            try {
                //val result = MoviesApi.moviesApiService.getMovies(1)
                //_moviesNowPlaying.value = result
                _status.value = ApiStatus.DONE
            }catch (e: Exception) {
                _status.value = ApiStatus.ERROR
                Log.d("ViewModelPlayingMovies", e.localizedMessage!!)
            }
        }
    }

    //get Movies with paging
    fun getMovies() : LiveData<PagingData<BodyMovies>>{
        return Pager(
            config = PagingConfig(
                pageSize = 20,
                enablePlaceholders = false,
            ),
            pagingSourceFactory = { MoviePagingSource(repository) }
        ).liveData.cachedIn(viewModelScope)
    }

}