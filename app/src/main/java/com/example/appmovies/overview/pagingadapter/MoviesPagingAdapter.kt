package com.example.appmovies.overview.pagingadapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.appmovies.R
import com.example.appmovies.adapter.CustomAdapter
import com.example.appmovies.model.BodyMovies

class MoviesPagingAdapter(
    val itemClickListener: ItemClickListener
) : PagingDataAdapter<BodyMovies, MoviesPagingAdapter.MovieViewHolder>(DIF_UTIL) {

    interface ItemClickListener {
        fun onItemClick(movies: BodyMovies)
    }

    class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val itemImage: ImageView
        val title: TextView
        val language: TextView
        val popularity: TextView

        init {
            itemImage = view.findViewById(R.id.item_image)
            title = view.findViewById(R.id.item_title)
            language = view.findViewById(R.id.item_language)
            popularity = view.findViewById(R.id.item_popularity)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = getItem(position)!!
        if (movie.poster_path == null) {
            holder.itemImage.load(R.drawable.ic_baseline_broken)
        } else {
            holder.itemImage.load("https://image.tmdb.org/t/p/w500" + movie.poster_path)
        }
        holder.title.text = movie.original_title
        holder.language.text = movie.original_language
        holder.popularity.text = movie.popularity.toString()
        holder.itemView.setOnClickListener {
            itemClickListener.onItemClick(movie)
        }
        /*holder.itemImage.load("https://image.tmdb.org/t/p/w500" + moviesList[position].poster_path)
        holder.title.text = moviesList[position].original_title
        holder.language.text = "Idioma: " +moviesList[position].original_language
        holder.popularity.text = "Popularidad: " + moviesList[position].popularity.toString()*/
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_item, parent, false)

        return MovieViewHolder(view)
    }

    object DIF_UTIL: DiffUtil.ItemCallback<BodyMovies>(){
        override fun areItemsTheSame(oldItem: BodyMovies, newItem: BodyMovies): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: BodyMovies, newItem: BodyMovies): Boolean {
            return oldItem == newItem
        }

    }
}