package com.example.appmovies.model

data class Videos(
    val id: Int,
    val results: List<Result>
)