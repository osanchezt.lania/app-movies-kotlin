package com.example.appmovies.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BodyMovies(
    val adult: Boolean? = false,
    val backdrop_path: String?,
    val genre_ids: List<Int>? = listOf(),
    val id: Int? = 0,
    val original_language: String?,
    val original_title: String?,
    val overview: String?,
    val popularity: Float? = 0.0f,
    val poster_path: String? = null,
    val release_date: String?,
    val title: String?,
    val video: Boolean? = false,
    val vote_average: Float? = 0.0f,
    val vote_count: Double?= 0.0
): Parcelable
