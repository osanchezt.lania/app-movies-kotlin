package com.example.appmovies.model

data class Movies(
    val dates: Dates,
    val page: Int,
    val results: List<BodyMovies>,
    val total_pages: Int,
    val total_results: Int
)

data class Dates(
    val maximum: String,
    val minimum: String
)