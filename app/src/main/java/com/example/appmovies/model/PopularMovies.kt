package com.example.appmovies.model

import com.squareup.moshi.Json

data class PopularMovies(
    val page: Int = 0,
    @Json(name = "results")
    val results: List<BodyMovies>,
    val total_pages: Int = 0,
    val total_results: Int = 0
)