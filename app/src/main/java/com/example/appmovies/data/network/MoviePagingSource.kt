package com.example.appmovies.data.network

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.appmovies.data.Repository
import com.example.appmovies.model.BodyMovies

class MoviePagingSource (
    private val repository: Repository,
        )
    : PagingSource<Int, BodyMovies>() {
    override fun getRefreshKey(state: PagingState<Int, BodyMovies>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            /*
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(-1)
            */

            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(-1)

        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, BodyMovies> {
        val page = params.key ?: 1
        return try {
            val data = repository.getPlayingMovies(page) //MoviesApi.moviesApiService.getMovies(page)
            //Log.d("PagingSource", data.body()?.results.toString())
            LoadResult.Page(
                data.body()!!.results,
                prevKey = if (page == 1) null else page - 1,
                nextKey = if (data.body()!!.results.isNotEmpty()) page + 1 else null
            )
        } catch (e: Exception) {
            e.message?.let { Log.e("PagingSource", it )}
            LoadResult.Error(e)
        }
    }
}