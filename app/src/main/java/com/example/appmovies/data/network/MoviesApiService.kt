package com.example.appmovies.data.network

import com.example.appmovies.model.DetailMovie
import com.example.appmovies.model.Movies
import com.example.appmovies.model.PopularMovies
import com.example.appmovies.model.Videos
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.concurrent.TimeUnit
/*
private const val BASE_URL =
    "https://api.themoviedb.org/3/movie/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private fun getLoggingHttpClient(): OkHttpClient {
    val timeOut = 90L
    val builder = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().apply {
            setLevel(HttpLoggingInterceptor.Level.BODY)
        })
        .connectTimeout(timeOut, TimeUnit.SECONDS)
        .callTimeout(timeOut, TimeUnit.SECONDS)
        .readTimeout(timeOut, TimeUnit.SECONDS)
        .writeTimeout(timeOut, TimeUnit.SECONDS)
    return builder.build()
}

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .client(getLoggingHttpClient())
    .baseUrl(BASE_URL)
    .build()
*/

interface MoviesApiService {

    @GET("now_playing?api_key=041f97db655ce0746ecb98325a352d35")
    suspend fun getMovies(
        @Query("page") page: Int
    ) : Response<Movies>

    @GET("popular?api_key=041f97db655ce0746ecb98325a352d35")
    suspend fun getPopularMovies() : Response<PopularMovies>

    //157336?api_key=041f97db655ce0746ecb98325a352d35&append_to_response=videos
    @GET("{movie_id}?api_key=041f97db655ce0746ecb98325a352d35")
    suspend fun getMovieById(
        @Path("movie_id") movie_id: String
    ) : Response<DetailMovie>

    @GET("{movie_id}/videos?api_key=041f97db655ce0746ecb98325a352d35")
    suspend fun getVideo(
        @Path("movie_id") movie_id: String
    ) : Response<Videos>
}

/*
//Singleton
object MoviesApi {
    val moviesApiService:MoviesApiService by lazy {
        retrofit.create(MoviesApiService::class.java)
    }
}

 */

