package com.example.appmovies.data

import com.example.appmovies.data.network.MoviesApiService
import com.example.appmovies.model.Videos
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Response
import javax.inject.Inject

@ActivityRetainedScoped
class Repository @Inject constructor(
    private val moviesApiService: MoviesApiService
) {

    suspend fun getPlayingMovies(page: Int) = moviesApiService.getMovies(page)

    suspend fun getPopularMovies() = flow {
        val popularMovies = moviesApiService.getPopularMovies()
        emit(popularMovies)
        /*
        while (true) {
            val popularMovies = moviesApiService.getPopularMovies()
            emit(popularMovies)
            delay(5000)
        }
        */
    }

    suspend fun getVideo(id: String) : Flow<Response<Videos>> = flow {
        emit(moviesApiService.getVideo(id))
    }
}