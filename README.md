App Movies

La app fue desarrollada bajo un patro de diseño MVVM. 
los puntos mencionados a continuación no fueron completados por cuestión de tiempo de entrega de dicha prueba:

*Visualización de videos en el detalle.
*Funcionar offline.

Para realizar la función de modo offline, implementaría una base de datos local con la librería ROOM para así 
guardar información una vez que la app se conecte a internet. 

*Crear base de datos [Entity]
*Crear DAO[Acceso a datos]
*Instancia de DB.
*ViewModel para leer, escribir información de la base de datos y exponerlos mediante LiveData para que puedan
	ser consumidos desde la interfaz(fragment).
*Mostrar información en el fragment desde el viewModel.